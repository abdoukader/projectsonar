package com.groupeisi.projectsonar.repository;


import com.groupeisi.projectsonar.model.Book;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class BookRepositoryTest {
    private static final String BOOK_TITLE = "The Little Prince";

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private BookRepository bookRepository;

    @Test
    void whenFindByTitle_thenReturnBook() {
        // GIVEN
        final Book book = new Book();
        book.setTitle(BOOK_TITLE);
        this.entityManager.persist(book);
        this.entityManager.flush();

        // WHEN
        final Book bookBDD = this.bookRepository.findByTitle(BOOK_TITLE);

        // THEN
        assertNotNull(bookBDD);
        assertEquals(BOOK_TITLE, bookBDD.getTitle());
    }
}

