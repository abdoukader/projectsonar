package com.groupeisi.projectsonar.service;


import com.groupeisi.projectsonar.model.Book;
import com.groupeisi.projectsonar.repository.BookRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
class BookServiceTest {
    @TestConfiguration
    static class BookServiceTestContextConfiguration {

        @Bean
        public BookService employeeService() {
            return new BookService();
        }
    }

    private static final String BOOK_TITLE = "The Little Prince";

    @Autowired
    private BookService employeeService;

    @MockBean
    private BookRepository bookRepository;

    @BeforeEach
    public void setUp() {
        final Book book = new Book();
        book.setTitle(BOOK_TITLE);

        Mockito.when(this.bookRepository.findByTitle(book.getTitle()))
                .thenReturn(book);
    }

    @Test
    void whenValidName_thenEmployeeShouldBeFound() {
        // GIVEN
        final String title = BOOK_TITLE;

        // WHEN
        final Book bookBDD = this.employeeService.getBookByTitle(title);

        // THEN
        assertEquals(title, bookBDD.getTitle());
    }
}
