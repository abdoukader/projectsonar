package com.groupeisi.projectsonar.controller;

import antlr.collections.List;
import com.groupeisi.projectsonar.model.Book;
import com.groupeisi.projectsonar.service.BookService;
import org.assertj.core.util.Arrays;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;

import static net.bytebuddy.matcher.ElementMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(BookController.class)
class BookControllerTest {
    private static final String BOOK_TITLE = "The Little Prince";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private BookService service;

    @Test
    void givenBooks_whenGetBooks_thenReturnJsonArray() throws Exception {
        // GIVEN
        final Book book = new Book();
        book.setTitle(BOOK_TITLE);

        final List books = (List) Arrays.asList(book);

        // WHEN
        Mockito.when(this.service.getAllBooks()).thenReturn((java.util.List<Book>) books);

        this.mvc.perform(get("/api/books")
                        // THEN
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect((ResultMatcher) jsonPath("$[0].title", is(book.getTitle())));
    }
}
