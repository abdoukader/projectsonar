package com.groupeisi.projectsonar.controller;



import com.groupeisi.projectsonar.model.Book;
import com.groupeisi.projectsonar.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class BookController {
    @Autowired
    private BookService bookService;

    @GetMapping(value = "/books")
    public List<Book> getAllBooks() {
        return this.bookService.getAllBooks();
    }
}

