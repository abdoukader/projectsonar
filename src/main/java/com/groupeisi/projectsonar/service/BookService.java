package com.groupeisi.projectsonar.service;

import com.groupeisi.projectsonar.model.Book;
import com.groupeisi.projectsonar.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {
    @Autowired
    private BookRepository bookRepository;

    public List<Book> getAllBooks() {
        return this.bookRepository.findAll();
    }

    public Book getBookByTitle(String title) {
        return this.bookRepository.findByTitle(title);
    }
}
