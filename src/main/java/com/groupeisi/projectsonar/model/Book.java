package com.groupeisi.projectsonar.model;

import jakarta.persistence.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
    @Entity
    @Table(name = "book")
    public class Book {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Long id;

        @Size(min = 1, max = 30)
        @NotNull
        private String title;

        public Long getId() {
            return this.id;
        }

        public String getTitle() {
            return this.title;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
