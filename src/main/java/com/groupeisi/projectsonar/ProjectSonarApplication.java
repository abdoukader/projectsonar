package com.groupeisi.projectsonar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectSonarApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectSonarApplication.class, args);
    }

}
